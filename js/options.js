function save_options() {
  var imdbOption = document.getElementById('imdb_option').checked;
  var icheckmoviesOption = document.getElementById('icheckmovies_option').checked;
  var contextmenuOption = document.getElementById('contextmenu_option').checked;

  chrome.storage.sync.set({
    'imdbOption': imdbOption,
    'icheckmoviesOption': icheckmoviesOption,
    'contextmenuOption': contextmenuOption
  }, function() {
    var status = document.getElementById('status');
    status.textContent = 'OPTIONS SAVED!';

    setTimeout(function() {
      status.textContent = '';
    }, 2500);
  });
}

function restore_options() {
  chrome.storage.sync.get({
    'imdbOption': false,
    'icheckmoviesOption': false,
    'contextmenuOption': false
  }, function(values) {
    document.getElementById('imdb_option').checked = values.imdbOption;
    document.getElementById('icheckmovies_option').checked = values.icheckmoviesOption;
    document.getElementById('contextmenu_option').checked = values.contextmenuOption;

    if (values.imdbOption) {
      document.getElementById('imdb_option_icon').parentElement.className += ' check';
    }

    if (values.icheckmoviesOption) {
      document.getElementById('icheckmovies_option_icon').parentElement.className += ' check';
    }

    if (values.contextmenuOption) {
      document.getElementById('contextmenu_option_icon').parentElement.className += ' check';
    }
  });
}

function toggle_imdb_option() {
  document.getElementById('imdb_option_icon').parentElement.classList.toggle('check');
  document.getElementById('imdb_option').checked = !document.getElementById('imdb_option').checked;
}

function toggle_icheckmovies_option() {
  document.getElementById('icheckmovies_option_icon').parentElement.classList.toggle('check');
  document.getElementById('icheckmovies_option').checked = !document.getElementById('icheckmovies_option').checked;
}

function toggle_contextmenu_option() {
  document.getElementById('contextmenu_option_icon').parentElement.classList.toggle('check');
  document.getElementById('contextmenu_option').checked = !document.getElementById('contextmenu_option').checked;
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('imdb_option_icon').addEventListener('click', toggle_imdb_option);
document.getElementById('icheckmovies_option_icon').addEventListener('click', toggle_icheckmovies_option);
document.getElementById('contextmenu_option_icon').addEventListener('click', toggle_contextmenu_option);
document.getElementById('save').addEventListener('click', save_options);
