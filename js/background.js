/**
  *  Background Page
  **/

/* Helpers */
function init() {
  /* State events */
  chrome.runtime.onInstalled.addListener(function(details) {
    if (details.reason == 'install') {
      console.log('App installed!');
    }

    if (details.reason == 'update') {
      console.log('App updated!');
    }
  });

  chrome.runtime.onStartup.addListener(function() {});

  chrome.runtime.onSuspend.addListener(function() {});

  chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    switch(request.messageType) {
    case 'init':
      sendResponse({message: 'OK'});
      break;
    }
  });

  /* Commands events */
  chrome.commands.onCommand.addListener(function(command) {
    if (command == 'switch_imdb_icheckmovies') {

    }
  });

  /* Tabs events */
  chrome.tabs.onCreated.addListener(function(tab) {});

  chrome.tabs.onRemoved.addListener(function(tabId, removeInfo) {});

  chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    if (tab.url.indexOf('imdb.com') > -1 && tab.url.match(/(tt\d+)/g) != null) {
      chrome.pageAction.show(tabId);
      chrome.pageAction.setTitle({'tabId': tabId, 'title': 'Open on iCheckMovies'});
    } else if (tab.url.indexOf('icheckmovies.com/movies/') > -1) {
      chrome.pageAction.show(tabId);
      chrome.pageAction.setTitle({'tabId': tabId, 'title': 'Open on IMDb.com'});
    }
  });

  /* Windows events */
  chrome.windows.onCreated.addListener(function(window) {});

  chrome.windows.onRemoved.addListener(function(windowId) {});

  chrome.windows.onFocusChanged.addListener(function(windowId) {});

  /* Page Action events */
  chrome.pageAction.onClicked.addListener(function(tab) {
    if (tab.url.indexOf('imdb.com') > -1 && tab.url.match(/(tt\d+)/g) != null) {
      var result = tab.url.match(/(tt\d+)/g);

      if (result != null) {
        chrome.storage.sync.get('icheckmoviesOption', function(values) {
          if (values.icheckmoviesOption) {
            chrome.tabs.create({'url': 'https://www.icheckmovies.com/search/movies/?query=' + result[0], 'index': tab.index + 1, 'active': true}, function(createdTab) {
              // Later: Go directly to the movie that matches the IMDb ID
            });
          } else {
            chrome.tabs.update(tab.id, {'url': 'https://www.icheckmovies.com/search/movies/?query=' + result[0]}, function(updatedTab) {
              // Later: Go directly to the movie that matches the IMDb ID
            });
          }
        });
      }
    } else if (tab.url.indexOf('icheckmovies.com/movies/') > -1) {
      var code = "document.querySelector('.external[href*=\"imdb.com/title/\"]').href";

      chrome.tabs.executeScript(tab.id, {'code': code}, function(result) {
        chrome.storage.sync.get('imdbOption', function(values) {
          if (values.imdbOption) {
            chrome.tabs.create({'url': result[0], 'index': tab.index + 1, 'active': true});
          } else {
            chrome.tabs.update(tab.id, {'url': result[0]});
          }
        });
      });
    }
  });

  /* Context Menus events */
  chrome.contextMenus.onClicked.addListener(function(info, tab) {
    if (info.selectionText != null) {
      var query = info.selectionText.toString().trim().split(' ').join('+');

      chrome.tabs.create({'url': 'https://www.icheckmovies.com/search/movies/?query=' + query, 'active': true});
    } else if (info.linkUrl != null) {
      if (info.linkUrl.indexOf('imdb.com') > -1) {
        var result = info.linkUrl.match(/(tt\d+)/g);

        if (result != null) {
          chrome.storage.sync.get('contextmenuOption', function(values) {
            if (values.contextmenuOption) {
              chrome.tabs.create({'url': 'https://www.icheckmovies.com/search/movies/?query=' + result[0], 'index': tab.index + 1, 'active': true}, function(createdTab) {
                // Later: Go directly to the movie that matches the IMDb ID
              });
            } else {
              chrome.tabs.update(tab.id, {'url': 'https://www.icheckmovies.com/search/movies/?query=' + result[0]}, function(updatedTab) {
                // Later: Go directly to the movie that matches the IMDb ID
              });
            }
          });
        }
      }
    }
  });
}

function initContextMenu() {
  chrome.contextMenus.create({'title': 'Search on iCheckMovies', 'contexts': ['selection', 'link']});
  // chrome.contextMenus.create({'title': 'Add to your Watchlist', 'contexts': ['link']});
}

init();
initContextMenu();
